package br.ita.ct200.automatabuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.ita.ct200.automatabuilder.exception.RegexException;

public class AutomatonTest {

	private Automaton ndfaWithE;
	private Automaton ndfa;

	@Before
	public void setUp() throws Exception {
		ndfaWithE = new NonDeterministicFiniteAutomatonWithEpsilonsTransitions(
				"dad*c*aa(a+b)*+aa(a+b)*c*(aa)*aa(a+b)*+(a+b*c)+c(a+bc+cc+c*)c+a(a*+bac*b*+c)a(c+a)+aa+bb+c*+c*bb*+a*+bbc*baa+bb*c*bb*+ca*c+ccaac*");
		
		ndfa = new NonDeterministicFiniteAutomaton(
				"dad+aa(a+b)*+(a+b*c)+c(a+bc+cc+c*)c+a(a*+bac*b*+c)a(c+a)+aa+bb+c*+c*bb*+a*+bbc*baa+bb*c*bb*+ca*c+ccaac*");
	}

	@After
	public void tearDown() throws Exception {
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRegexToNDFAWithETransitions() throws RegexException, FileNotFoundException, IOException {

		String[] lines1 = ndfaWithE.toGraphvizDotFormat().split(System.getProperty("line.separator"));
		
//		ndfaWithE.render();
		
		List<String> lines2 = IOUtils.readLines(new FileInputStream("resources/automata1.txt"));
		
//		assertEquals(lines1.length, lines2.size());
		
		ndfaWithE.save(new File("/tmp/automata.png"));
		
//		for(int i = 0; i < lines1.length; i++){
//			assertEquals(lines1[i], lines2.get(i));
//		}
		
	
	}
	
	@Test
	public void testRegexToNDFA() throws RegexException, FileNotFoundException, IOException {

		String[] lines1 = ndfa.toGraphvizDotFormat().split(System.getProperty("line.separator"));
		
		System.out.println(ndfa.toGraphvizDotFormat());
		
		@SuppressWarnings("unchecked")
		List<String> lines2 = IOUtils.readLines(new FileInputStream("resources/automata2.txt"));
		
//		assertEquals(lines1.length, lines2.size());
		
		ndfaWithE.save(new File("/tmp/automata.png"));
		
//		for(int i = 0; i < lines1.length; i++){
//			assertEquals(lines1[i], lines2.get(i));
//		}
		
	}

	@Test
	public void testProcessString() throws RegexException {

		Automaton automata = new NonDeterministicFiniteAutomatonWithEpsilonsTransitions("dad*c+cc*a+c");
		
		ProcessingString result = automata.processString("daddddddddddc");
		assertTrue(result.isAcceptable());
		assertEquals(1, result.getStatesOnTheWay().size());
		assertTrue(result.getStatesOnTheWay().contains(1));
		
		result = automata.processString("ddd");
		assertFalse(result.isAcceptable());
//		assertEquals(1, result.getStatesOnTheWay().size());
//		assertTrue(result.getStatesOnTheWay().contains(2));

	}

	@Test
	public void testAlphabetCreation1() throws RegexException {
			Automaton aut = new NonDeterministicFiniteAutomatonWithEpsilonsTransitions("abx11+(b+c)*");
			
			assertTrue(aut.getAlphabet().contains('a'));
			assertTrue(aut.getAlphabet().contains('1'));
			assertTrue(aut.getAlphabet().contains('b'));
			assertTrue(aut.getAlphabet().contains('c'));
			assertTrue(aut.getAlphabet().contains('x'));
			

	}
	
	@Test
	public void testFailOnCreation1() {
		try {
			new NonDeterministicFiniteAutomatonWithEpsilonsTransitions("((a+b)");
		} catch (RegexException e) {
			return;
		}

		fail("Deveria ter falhado pois a regex passada não está adequada.");

	}

	@Test
	public void testFailOnCreation2() {
		try {
			new NonDeterministicFiniteAutomatonWithEpsilonsTransitions(null);
		} catch (RegexException e) {
			return;
		}

		fail("Deveria ter falhado pois a regex passada não está adequada.");

	}

}
