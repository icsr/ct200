package br.ita.ct200.automatabuilder.tools;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.ita.ct200.automatabuilder.support.Tools;

public class ToolsTest {

	private String[] unions;
	private String[] star;

	@Before
	public void setUp() throws Exception {
		unions = new String[]{"a+b","aaa+bbbb+cccc","a*+bb+cc+ab*", "a(b+c)+(a+c)*", "a(b+cc)dd","(aaa+bbbb)cccc", "a(b+c)(a*+c)*", "a*+(bb+c)c+(ab*)"};
		
		star = new String[]{"aaab*ccaa","(aaaabb)+"};
	
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSplitUnions() {
		List<String> list = Tools.splitUnionsOutOfParenthesis(unions[0]);
		assertEquals(2, list.size());
		assertEquals("a", list.get(0));
		assertEquals("b", list.get(1));
		
		list = Tools.splitUnionsOutOfParenthesis(unions[1]);
		assertEquals(3, list.size());
		assertEquals("aaa", list.get(0));
		assertEquals("bbbb", list.get(1));
		assertEquals("cccc", list.get(2));
		
		list = Tools.splitUnionsOutOfParenthesis(unions[2]);
		assertEquals(4, list.size());
		assertEquals("a*", list.get(0));
		assertEquals("bb", list.get(1));
		assertEquals("cc", list.get(2));
		assertEquals("ab*", list.get(3));
		
		list = Tools.splitUnionsOutOfParenthesis(unions[3]);
		assertEquals(2, list.size());
		assertEquals("a(b+c)", list.get(0));
		assertEquals("(a+c)*", list.get(1));
		
	}

}
