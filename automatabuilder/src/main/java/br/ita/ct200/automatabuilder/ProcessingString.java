package br.ita.ct200.automatabuilder;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import br.ita.ct200.automatabuilder.exception.RegexException;
import br.ita.ct200.automatabuilder.support.Constant;
import br.ita.ct200.automatabuilder.support.Tools;
import br.ita.ct200.automatabuilder.support.Transition;

public class ProcessingString {

	private String regex;
	private Automaton automaton;
	private HashSet<Integer> statesOnTheWay;

	public ProcessingString(String regex, Automaton automaton) throws RegexException {
		
		regex = regex.replaceAll(""+Constant.EPSILON, "");
		
		Tools.validate(regex, automaton.alphabet, false);
		
		this.regex = regex;
		this.automaton = automaton;
		this.statesOnTheWay = new HashSet<Integer>();

		process(0, Tools.toSet(automaton.getInitialState()));
	}

	private void process(int regexIndex, Set<Integer> fromStates) {
		
		
		if(fromStates.isEmpty()){
			return;
		}
		
		Set<Integer> nextFromStates = new HashSet<Integer>();
		Set<Integer> nextFromStatesWithIncrementedRegexIndex = new HashSet<Integer>();
		Set<Integer> reposition = new HashSet<Integer>();

		for (Integer fromState : fromStates) {

			if (regexIndex >= regex.length()) {

				Set<Integer> ecloses = automaton.transitions.eClose(fromState);
				this.statesOnTheWay.addAll(ecloses);
				nextFromStates.addAll(ecloses);
			}

			LinkedList<Transition> fromStateTransitions = automaton.transitions.findTransitionsFromState(fromState);

			for (Transition fromStateTransition : fromStateTransitions) {

				if (regexIndex < regex.length() && fromStateTransition.expressionEquals(regex.charAt(regexIndex))) {
					nextFromStatesWithIncrementedRegexIndex.add(fromStateTransition.getToState());
					if(automaton.isCycle(fromStateTransition)){
						reposition.add(fromStateTransition.getToState());
					}
						
				}
				if (fromStateTransition.expressionEqualsEpsilon()) {
					nextFromStates.add(fromStateTransition.getToState());
				}

			}

		}

		nextFromStates.removeAll(fromStates);
		nextFromStatesWithIncrementedRegexIndex.removeAll(fromStates);
		nextFromStatesWithIncrementedRegexIndex.addAll(reposition);
		process(regexIndex, nextFromStates);
		process(regexIndex + 1, nextFromStatesWithIncrementedRegexIndex);

	}

	public boolean isAcceptable() {
		boolean status = false;

		Iterator<Integer> its = statesOnTheWay.iterator();
		Iterator<Integer> itfs = automaton.finalStates.iterator();

		while (its.hasNext()) {
			Integer i = its.next();
			while (itfs.hasNext()) {
				if (i.equals(itfs.next())) {
					status = true;
				}
			}
		}

		return status;
	}

	public Set<Integer> getStatesOnTheWay() {
		return this.statesOnTheWay;
	}

}
