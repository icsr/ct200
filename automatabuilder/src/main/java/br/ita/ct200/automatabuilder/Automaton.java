package br.ita.ct200.automatabuilder;

import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import br.ita.ct200.automatabuilder.exception.RegexException;
import br.ita.ct200.automatabuilder.support.Constant;
import br.ita.ct200.automatabuilder.support.Tools;
import br.ita.ct200.automatabuilder.support.Transition;
import br.ita.ct200.automatabuilder.support.TransitionComposite;
import br.ita.ct200.automatabuilder.support.view.Graph;

public abstract class Automaton {

	protected String regex;

	protected LinkedList<Integer> states;

	protected Set<Character> alphabet;

	protected LinkedList<Integer> finalStates;

	protected TransitionComposite transitions;

	protected String description;

	protected Integer finalState;

	public Automaton(String regex, String description, Set<Character> alphabet) throws RegexException {

		this.transitions = new TransitionComposite();
		states = new LinkedList<Integer>();
		finalStates = new LinkedList<Integer>();
		this.description = description;

		buildAlphabet(regex, alphabet);

		Tools.validate(regex, this.alphabet, true);

		setRegex(regex);

		build();

	}

	protected abstract void build();

	protected void buildAlphabet(String regex, Set<Character> alphabet) throws RegexException {

		if (regex == null) {
			throw new RegexException("Regex nula");
		}

		if (alphabet == null) {
			alphabet = new HashSet<Character>();
			char[] chars = regex.toCharArray();
			for (char c : chars) {
				if (c != Constant.CLOSE_PARENTHESIS && c != Constant.OPEN_PARENTHESIS && c != Constant.EPSILON
						&& c != Constant.STAR && c != Constant.UNION) {
					alphabet.add(c);
				}
			}
		}

		this.alphabet = alphabet;
	}

	protected Integer createState(boolean isFinal) {

		int last = states.size() > 0 ? states.getLast() + 1 : 0;
		states.add(last);
		if (isFinal) {
			finalStates.add(states.getLast());
		}

		return states.getLast();
	}

	protected void destroyState(Integer id) {

		while (states.contains(id)) {
			states.remove(id);
		}

		while (finalStates.contains(id)) {
			finalStates.remove(id);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ita.israel.automata.automatabuilder.Automatable#getStates()
	 */
	public LinkedList<Integer> getStates() {
		return states;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ita.israel.automata.automatabuilder.Automatable#getFinalStates()
	 */
	public LinkedList<Integer> getFinalStates() {
		return finalStates;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ita.israel.automata.automatabuilder.Automatable#getAlphabet()
	 */
	public Set<Character> getAlphabet() {
		return alphabet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ita.israel.automata.automatabuilder.Automatable#getTransitions()
	 */
	public List<Transition> getTransitions() {
		return transitions.getTransitions();
	}

	public Integer getFinalState() {
		return finalState;
	}

	public Integer getInitialState() {
		return initialState;
	}

	protected Integer initialState;

	public Automaton(String regex, String description) throws RegexException {
		this(regex, description, null);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ita.israel.automata.automatabuilder.Automatable#getRegex()
	 */
	public String getRegex() {
		return regex;
	}

	private void setRegex(String regex) {
		this.regex = regex;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.ita.israel.automata.automatabuilder.Automatable#processString(java.
	 * lang.String)
	 */
	public ProcessingString processString(String string) throws RegexException {
		return new ProcessingString(string, this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ita.israel.automata.automatabuilder.Automatable#render()
	 */
	public File render() {
		Graph g = new Graph(this);

		File file = new File(String.format("%s%s_%s.png", Constant.TMP_FOLDER, File.separator,
				this.getDescription().replaceAll("" + Constant.EPSILON, "Epsilon")));
		g.renderToFile(file);

		return file;
	}

	public String toGraphvizDotFormat() {
		return new Graph(this).toDotGraphvizFormat();
	}

	public void save(File pngFile) {
		new Graph(this).renderToFile(pngFile);
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return getDescription();
	}

	protected boolean hasNoUnionsOuterOfParenthesis(String regex) {
		return Tools.findOcurrenciesOutOfParenthesis(regex, Constant.UNION).isEmpty();
	}

	protected abstract void createTransition(String expression, Integer fromState, Integer toState);

	protected boolean isParenthesisExpressionParenthesis(String expression) {
		return Pattern.matches("\\(.*\\)", expression);
	}

	public boolean isCycle(Transition transition) {
		return isCycle(transition, transition.getFromState());
	}

	public boolean isCycle(Transition transition, Integer startState) {
		if (transition.isLoop()) {
			return true;
		}

		for (Transition t : lastTransitionsBefore(transition, startState)) {
			if (startState == t.getToState()) {
				return true;
			}
		}
		return false;

	}

	int checkLoopCounter = 0;
	public Set<Transition> lastTransitionsBefore(Transition transition, Integer startState) {

		Set<Transition> list = loopLastTransitionsBefore(transition, startState);
		this.checkLoopCounter = 0;
		return list;

	}

	private Set<Transition> loopLastTransitionsBefore(Transition transition, Integer startState) {
		Set<Transition> lastTransitions = new HashSet<Transition>();
		List<Transition> nexts = transitions.findTransitionsFromState(transition.getToState());
		
		for (Transition next : nexts) {

			if (next.getToState() == startState) {
				lastTransitions.add(next);
			} else if (!next.isLoop() && ++this.checkLoopCounter < states.size()) {
				lastTransitions.addAll(loopLastTransitionsBefore(next, startState));
			}
		}

		return lastTransitions;
	}

}