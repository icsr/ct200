package br.ita.ct200.automatabuilder.support;

public class Transition {
	
	String expression;
	Integer fromState;
	Integer toState;
	
	public String getExpression() {
		return expression;
	}
	public Integer getFromState() {
		return fromState;
	}
	public Integer getToState() {
		return toState;
	}
	
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public void setFromState(Integer fromState) {
		this.fromState = fromState;
	}
	public void setToState(Integer toState) {
		this.toState = toState;
	}
	
	public boolean expressionContains(char character){
		return expression.contains(""+character);
	}
	
	public boolean expressionEqualsEpsilon(){
		return expressionContains(Constant.EPSILON);
	}
	
	public boolean expressionEquals(char character){
		return expression.equals(""+character);
	}
	
	@Override
	public int hashCode() {
		return 100*expression.hashCode() + 10*fromState.hashCode() + toState.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		
		try{
			Transition o = (Transition) obj;
			return this.expression.equals(o.expression) && this.fromState.equals(o.fromState) && this.toState.equals(o.toState);
		}catch(ClassCastException e){
			return false;
		}
		
	}
	
	public boolean isLoop(){
		return fromState.equals(toState);
	}

}
