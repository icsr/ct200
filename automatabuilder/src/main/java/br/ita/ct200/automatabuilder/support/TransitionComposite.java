package br.ita.ct200.automatabuilder.support;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class TransitionComposite extends Transition implements Iterable<Transition> {

	LinkedList<Transition> transitions = new LinkedList<Transition>();

	public TransitionComposite() {
	}

	private TransitionComposite(String expression, Integer fromState, Integer toState) {
		this.expression = expression;
		this.fromState = fromState;
		this.toState = toState;
	}

	public void add(String expression, Integer fromState, Integer toState) {

		TransitionComposite t = new TransitionComposite(expression, fromState, toState);

		if (!transitions.contains(t)) {
			transitions.add(t);
		}
	}

	public LinkedList<Transition> findTransitionsFromState(Integer fromState) {

		LinkedList<Transition> list = new LinkedList<Transition>();

		for (Transition t : transitions) {
			if (t.fromState.equals(fromState)) {
				list.add(t);
			}
		}

		return list;

	}

	public LinkedList<Transition> findTransitions(String expression) {

		LinkedList<Transition> list = new LinkedList<Transition>();

		for (Transition t : transitions) {
			if (t.expression.equals(expression)
					|| (expression.equals("" + Constant.EPSILON) && Pattern.matches("\\&[0-9]*", t.expression))) {
				list.add(t);
			}
		}

		return list;
	}

	public LinkedList<Transition> findTransitions(String expression, Integer fromState) {

		LinkedList<Transition> list = new LinkedList<Transition>();

		for (Transition t : transitions) {
			if (t.fromState.equals(fromState) && (t.expression.equals(expression)
					|| (expression.equals("" + Constant.EPSILON) && Pattern.matches("\\&[0-9]*", t.expression)))) {
				list.add(t);
			}
		}

		return list;
	}

	public LinkedList<Transition> findTransitions(String expression, Integer fromState, Integer toState) {

		LinkedList<Transition> list = new LinkedList<Transition>();

		for (Transition t : transitions) {
			if (t.toState.equals(toState) && t.fromState.equals(fromState) && (t.expression.equals(expression)
					|| (expression.equals("" + Constant.EPSILON) && Pattern.matches("\\&[0-9]*", t.expression)))) {
				list.add(t);
			}
		}

		return list;
	}

	public LinkedList<Transition> findTransitions(Integer fromState, Integer toState) {

		LinkedList<Transition> list = new LinkedList<Transition>();

		for (Transition t : transitions) {
			if (t.toState.equals(toState) && t.fromState.equals(fromState)) {
				list.add(t);
			}
		}

		return list;
	}

	int counter = 0;
	public LinkedList<Transition> findTransitions(Integer fromState, Integer toState, boolean transitively) {
		if (!transitively) {
			return findTransitions(fromState, toState);
		}

		LinkedList<Transition> list = new LinkedList<Transition>();
		Iterator<Transition> transitionsIt = transitions.iterator();
		
		while(transitionsIt.hasNext()){
			Transition nextTransition = transitionsIt.next();
			
			if(nextTransition.getToState().equals(toState)){
				list.add(nextTransition);
			}
			
		}
		
		for(Transition trans : list){
			
			if(trans.getFromState().equals(fromState))
				continue;
			
			if(counter++ > transitions.size()){
				counter = 0;
				return list;
			}
			
			LinkedList<Transition> aux = findTransitions(fromState, trans.getFromState(), transitively);

			boolean toRemove = true;
			for(Transition a : aux){
				if(a.getFromState().equals(fromState)){
					toRemove = false;
					break;
				}
			}
			
			if(toRemove){
				list.remove(trans);
			}
			
		}
		
		return list;


	}

	public LinkedList<Transition> findEpsilonTransitions(Integer fromState, Integer toState) {

		LinkedList<Transition> list = new LinkedList<Transition>();

		for (Transition t : transitions) {
			if (t.toState.equals(toState) && t.fromState.equals(fromState) && t.expressionEqualsEpsilon()) {
				list.add(t);
			}
		}

		return list;
	}

	public LinkedList<Transition> findEpsilonsTransitions(Integer actualState) {
		return findTransitions("" + Constant.EPSILON, actualState);
	}

	public LinkedList<Transition> findEpsilonsTransitions() {
		return findTransitions("" + Constant.EPSILON);
	}

	public LinkedList<Transition> getTransitions() {
		return transitions;
	}

	public Set<Integer> eClose(Integer actualState, boolean transtively) {
		HashSet<Integer> h = new HashSet<Integer>();
		h.add(actualState);

		eClose(actualState, h, transtively);

		return h;
	}

	public Set<Integer> eClose(Integer actualState) {
		return eClose(actualState, false);
	}

	private void eClose(Integer actualState, HashSet<Integer> eclose, boolean transtively) {

		LinkedList<Transition> trans = findEpsilonsTransitions(actualState);

		for (Transition t : trans) {
			eclose.add(t.toState);
			if (transtively) {
				eClose(t.toState, eclose, transtively);
			}
		}

	}

	public void remove(Transition transition) {
		transitions.remove(transition);
	}

	public Transition getLast() {
		return getTransitions().getLast();
	}

	public Transition getFirst() {
		return getTransitions().getFirst();
	}

	public Iterator<Transition> iterator() {

		return new TransitionIterator();
	}

	class TransitionIterator implements Iterator<Transition> {

		int i = 0;

		public boolean hasNext() {
			return i < transitions.size();
		}

		public Transition next() {
			return transitions.get(i++);
		}

		public void remove() {
			transitions.remove(i);
		}

	}

	public List<Transition> findTransitionsToState(Integer toState) {
		
		List<Transition> list = new LinkedList<Transition>();
		
		for(Transition t : transitions){
			if(t.getToState().equals(toState)){
				list.add(t);
			}
		}
		
		return list;
	}

}
