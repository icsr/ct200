package br.ita.ct200.automatabuilder.support.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import br.ita.ct200.automatabuilder.NonDeterministicFiniteAutomaton;
import br.ita.ct200.automatabuilder.NonDeterministicFiniteAutomatonWithEpsilonsTransitions;
import br.ita.ct200.automatabuilder.ProcessingString;
import br.ita.ct200.automatabuilder.exception.AbortException;
import br.ita.ct200.automatabuilder.exception.RegexException;
import br.ita.ct200.automatabuilder.support.Constant;
import br.ita.ct200.automatabuilder.support.Tools;

public class Window extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3916995800496666719L;
	private JLabel label;
	private JTextField regex;
	private GraphPanel rightBottomMainPanel;
	private GraphPanel leftBottomMainPanel;
	private GraphPanel leftBottomRegexPanel;
	protected NonDeterministicFiniteAutomatonWithEpsilonsTransitions automaton1;
	protected NonDeterministicFiniteAutomaton automaton2;
	private JLabel leftTopMainLabel;
	private JLabel leftTopRegexMainLabel;
	private JLabel rightTopMainLabel;
	private JScrollPane rightBottomScrollPane;
	private JScrollPane leftBottomScrollPane;
	private JScrollPane leftBottomRegexScrollPane;
	private JPanel leftMainPanel;
	private JPanel leftRegexPanel;
	private JPanel rightMainPanel;
	private JSplitPane main;
	private JSplitPane leftSide;

	public Window() {
		render();
		updateMainPanel();
	}

	private void render() {

		setLayout(new BorderLayout());
		setSize(Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height);
		setLocationByPlatform(true);
		setLocation(0, 0);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		setLayout(new BorderLayout());
		setTitle(Constant.APP_TITLE);

		JPanel topPanel = new JPanel(new BorderLayout());

		this.regex = new JTextField(Constant.DEFAULT_REGEX);
		this.regex.setFont(Constant.DEFAULT_FONT);
		this.regex.setHorizontalAlignment(SwingConstants.CENTER);
		topPanel.add(regex, BorderLayout.CENTER);

		JButton generateButton = new JButton("Gerar AFND");
		JButton processStringButton = new JButton("Verificar String");
		JButton exitButton = new JButton("Sair");
		JPanel line1ButtonsPanel = new JPanel();
		line1ButtonsPanel.add(generateButton);
		line1ButtonsPanel.add(processStringButton);
		line1ButtonsPanel.add(exitButton);
		topPanel.add(line1ButtonsPanel, BorderLayout.EAST);

		JPanel bottomPanel = new JPanel();

		this.label = new JLabel();
		bottomPanel.add(label);

		JPanel line2StatusPanel = new JPanel();
		line2StatusPanel.add(label);
		bottomPanel.add(line2StatusPanel, BorderLayout.CENTER);

		this.add(topPanel, BorderLayout.NORTH);
		this.add(bottomPanel, BorderLayout.SOUTH);

		main = new JSplitPane();
		leftSide = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		leftSide.setResizeWeight(0.85d);

		leftMainPanel = new JPanel(new BorderLayout());
		leftRegexPanel = new JPanel(new BorderLayout());
		rightMainPanel = new JPanel(new BorderLayout());

		this.leftTopMainLabel = new JLabel();
		this.leftTopMainLabel.setFont(Constant.DEFAULT_FONT);
		this.leftTopMainLabel.setBackground(Color.WHITE);
		this.leftTopMainLabel.setHorizontalAlignment(JLabel.CENTER);
		this.leftTopMainLabel.setOpaque(true);

		this.leftTopRegexMainLabel = new JLabel();
		this.leftTopRegexMainLabel.setFont(Constant.DEFAULT_FONT);
		this.leftTopRegexMainLabel.setBackground(Color.WHITE);
		this.leftTopRegexMainLabel.setHorizontalAlignment(JLabel.CENTER);
		this.leftTopRegexMainLabel.setOpaque(true);

		this.rightTopMainLabel = new JLabel();
		this.rightTopMainLabel.setFont(Constant.DEFAULT_FONT);
		this.rightTopMainLabel.setBackground(Color.WHITE);
		this.rightTopMainLabel.setHorizontalAlignment(JLabel.CENTER);
		this.rightTopMainLabel.setOpaque(true);

		this.leftBottomMainPanel = new GraphPanel();
		this.leftBottomRegexPanel = new GraphPanel();
		this.rightBottomMainPanel = new GraphPanel();

		leftMainPanel.add(leftTopMainLabel, BorderLayout.NORTH);
		leftMainPanel.add(leftBottomMainPanel, BorderLayout.CENTER);

		leftRegexPanel.add(leftTopRegexMainLabel, BorderLayout.NORTH);
		leftRegexPanel.add(leftBottomRegexPanel, BorderLayout.CENTER);

		rightMainPanel.add(rightTopMainLabel, BorderLayout.NORTH);
		rightMainPanel.add(rightBottomMainPanel, BorderLayout.CENTER);

		updateMainPane();
		main.setResizeWeight(0.5d);

		this.add(main, BorderLayout.CENTER);

		exitButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				exit();
			}

		});

		generateButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				updateMainPanel();

			}
		});

		this.regex.addKeyListener(new KeyListener() {

			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			public void keyReleased(KeyEvent e) {

			}

			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					updateMainPanel();
				}
			}
		});

		processStringButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				showProcessScreen();
			}
		});

		this.setVisible(true);
	}

	private void exit() {
		System.exit(0);
	}

	public void updateMainPanel() {
		try {
			this.label.setText("Processando...");
			this.automaton1 = new NonDeterministicFiniteAutomatonWithEpsilonsTransitions(regex.getText());
			this.automaton2 = new NonDeterministicFiniteAutomaton(regex.getText());

			this.leftTopMainLabel.setText(this.automaton1.getDescription());
			this.leftTopRegexMainLabel.setText("Regex : " + this.automaton1.getDescription());
			this.rightTopMainLabel.setText(this.automaton2.getDescription());

			this.leftBottomMainPanel.render(this.automaton1.render());
			this.rightBottomMainPanel.render(this.automaton2.render());
			this.leftBottomRegexPanel.render(Tools.getRegexFromAutomaton(automaton1).render());

			this.label.setText("Pronto.");

			updateMainPane();

		} catch (RegexException e1) {
			Tools.showMessage(e1.getMessage());
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private void updateMainPane() {

		leftBottomScrollPane = new JScrollPane(leftMainPanel);
		leftBottomRegexScrollPane = new JScrollPane(leftRegexPanel);
		rightBottomScrollPane = new JScrollPane(rightMainPanel);

		leftSide.setLeftComponent(leftBottomScrollPane);
		leftSide.setRightComponent(leftBottomRegexScrollPane);

		main.setLeftComponent(leftSide);
		main.setRightComponent(rightBottomScrollPane);

	}

	private void showProcessScreen() {
		try {
			while (true) {
				processStringAndShowResults(getStringToProcessOnAutomaton());
			}
		} catch (AbortException e) {
			System.out.println(e.getLocalizedMessage());
		}
	}

	private static String getStringToProcessOnAutomaton() {
		return Tools.showInputMessage("Digite uma cadeia para verificar sua aceitabilidade");
	}

	private void processStringAndShowResults(String string) throws AbortException {

		if (string == null) {
			throw new AbortException("Cancelado pelo usuário");
		}

		if (string.length() == 0) {
			string = "" + Constant.EPSILON;
		}

		try {
			ProcessingString result = automaton1.processString(string);

			StringBuilder str = new StringBuilder();
			str.append("A cadeia '").append(string).append("' ").append(result.isAcceptable() ? "" : "NÃO")
					.append(" foi aceita.\n");

			Iterator<Integer> it = result.getStatesOnTheWay().iterator();

			if (it.hasNext()) {
				str.append(String.format("Os possíveis estados no %s são:\n", automaton1));
			} else {
				str.append(String.format("Nenhum estado possivel no %s.", automaton1));
			}
			while (it.hasNext()) {
				str.append(it.next());
				if (it.hasNext()) {
					str.append(", ");
				}
			}

			Tools.showMessage(str.toString());

		} catch (RegexException e) {
			Tools.showMessage("Erro ao processar expressão");
			System.err.println(e.getLocalizedMessage());
		}
	}

}
