package br.ita.ct200.automatabuilder.support.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class GraphPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -24251916868824356L;
	private BufferedImage img;
	private int height;
	private int width;
	private Point center;

	public GraphPanel() {
	}

	public GraphPanel(File pngFile) throws IOException {
		render(pngFile);
	}

	public void render(File pngFile) throws IOException {
		img = ImageIO.read(pngFile);
		width = Toolkit.getDefaultToolkit().getScreenSize().width;
		height = Toolkit.getDefaultToolkit().getScreenSize().height;
		center = center(new Dimension(width, height), new Dimension(img.getWidth(), img.getHeight()));
		repaint();
	}

	public static Point center(Dimension screenDimension, Dimension imageDimension) {

		double x1 = 0;
		double y1 = 0;

		return new Point((int) x1, (int) y1);
	}

	@Override
	public Dimension getPreferredSize() {
		if (img != null) {
			return new Dimension(img.getWidth(), img.getHeight());
		} else {
			return new Dimension(width, height);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);
		if (img != null) {
			System.out.println(center.x + center.y);
			g.drawImage(img, 0, 0, img.getWidth(), img.getHeight(), null);
		}

	}

}
