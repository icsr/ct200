package br.ita.ct200.automatabuilder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import br.ita.ct200.automatabuilder.exception.RegexException;
import br.ita.ct200.automatabuilder.support.Constant;
import br.ita.ct200.automatabuilder.support.Transition;

public class NonDeterministicFiniteAutomaton extends NonDeterministicFiniteAutomatonWithEpsilonsTransitions {

	public NonDeterministicFiniteAutomaton(String regex) throws RegexException {
		super(regex);

		this.description = "AFN";
	}

	@Override
	protected void build() {
		super.build();

		for (int i : states) {
			removeEpsilonsTransitions(i);
		}

		removeAnomalies();

	}

	private void removeAnomalies() {

		List<Transition> toRemove = new ArrayList<Transition>();
		for(Transition t1 : transitions){
			
			if(t1.getFromState().equals(initialState)){
				continue;
			}
			boolean remove = true;
			for(Transition t2 : transitions){
				if(t2.getToState().equals(t1.getFromState()) && !t1.getFromState().equals(t2.getFromState())){
					remove = false;
					break;
				}
			}
			if(remove){
				toRemove.add(t1);
				destroyState(t1.getFromState());
			}
			
		}
		
		cleanTransitions(toRemove);

	}

	private void removeEpsilonsTransitions(int actualState) {

		Set<Integer> directEcloses = transitions.eClose(actualState, false);
		Set<Integer> indirectEcloses = transitions.eClose(actualState, true);

		LinkedList<Transition> toRemove = new LinkedList<Transition>();
		LinkedList<Transition> noLoopTransitions = new LinkedList<Transition>();
		LinkedList<Transition> loopTransitions = new LinkedList<Transition>();

		boolean actualStateIsFinalState = transformToFinalStateIfAppropriate(actualState, indirectEcloses);

		for (Integer eclose : directEcloses) {

			if (eclose == actualState) {
				continue;
			}

			Set<Integer> onTheWayEcloses = performIntersectionBetweenDirectAndIndirectEclosesToFindOnTheWayEcloses(
					actualState, directEcloses, indirectEcloses, eclose);

			fillNoloopAndLoopLists(actualState, noLoopTransitions, loopTransitions, onTheWayEcloses);

			if (noLoopTransitions.isEmpty() && actualStateIsFinalState) {
				moveLoopTransitionToActualState(actualState, toRemove, loopTransitions);
			} else {
				createTransitionsBetweenLoopsAndBeetwenLoopsAndNoloops(actualState, noLoopTransitions, loopTransitions,
						eclose);

			}

			cleanEpsilonsAndUnusedTransitions(onTheWayEcloses, toRemove);
			loopTransitions.clear();
			noLoopTransitions.clear();
			toRemove.clear();
		}

	}

	private Set<Integer> performIntersectionBetweenDirectAndIndirectEclosesToFindOnTheWayEcloses(int actualState,
			Set<Integer> directEcloses, Set<Integer> indirectEcloses, Integer eclose) {

		Set<Integer> eclosesOnTheWay = new HashSet<Integer>();
		eclosesOnTheWay.addAll(indirectEcloses);

		for (Integer i : directEcloses) {
			if (i != eclose && i != actualState) {
				eclosesOnTheWay.removeAll(transitions.eClose(i, true));
			}
		}
		return eclosesOnTheWay;
	}

	private boolean transformToFinalStateIfAppropriate(int state, Set<Integer> indirectEcloses) {
		boolean isStateAFinalState = false;
		for (Integer i : indirectEcloses) {
			if (finalStates.contains(i)) {
				finalStates.add(state);
				isStateAFinalState = true;
				break;
			}
		}
		return isStateAFinalState;
	}

	private void cleanEpsilonsAndUnusedTransitions(Set<Integer> eclosesOnTheWay, LinkedList<Transition> toRemove) {
		for (Integer ecloseOnTheWay : eclosesOnTheWay) {
			toRemove.addAll(transitions.findEpsilonsTransitions(ecloseOnTheWay));
		}

		cleanTransitions(toRemove);

	}

	private void cleanTransitions(List<Transition> toRemove) {
		for (Transition t : toRemove) {
			transitions.remove(t);
		}
	}

	private void createTransitionsBetweenLoopsAndBeetwenLoopsAndNoloops(Integer actualState,
			LinkedList<Transition> noLoopTransitions, LinkedList<Transition> loopTransitions, Integer eclose) {
		for (Transition loop : loopTransitions) {

			createTransition(loop.getExpression(), actualState, loop.getToState());

			Set<Integer> indirectEcloses = transitions.eClose(actualState);

			if (indirectEcloses.contains(eclose)) {
				transformToFinalStateIfAppropriate(eclose, indirectEcloses);
			}

			transformToFinalStateIfAppropriate(loop.getToState(), transitions.eClose(loop.getToState(), true));

			for (Transition loop2 : loopTransitions) {
				LinkedList<Transition> crossedLoopTransitions = transitions.findTransitions(loop.getToState(),
						loop2.getToState());

				for (Transition crossedLoopTransition : crossedLoopTransitions) {
					if (!crossedLoopTransition.expressionContains(Constant.EPSILON)) {
						continue;
					}

					LinkedList<Transition> l2 = transitions.findTransitions(loop2.getToState(), loop2.getToState());
					createTransition(l2.getFirst().getExpression(), loop.getToState(), loop2.getToState());
				}

			}

			for (Transition noloopTransition : noLoopTransitions) {
				createTransition(noloopTransition.getExpression(), actualState, noloopTransition.getToState());
			}

		}

	}

	private void moveLoopTransitionToActualState(Integer actualState, LinkedList<Transition> toRemove,
			LinkedList<Transition> loopTransitions) {
		for (Transition loop : loopTransitions) {

			if (loop.isLoop()) {
				createTransition(loop.getExpression(), actualState, actualState);
				continue;
			}

			if (!isCycle(loop)) {
				return;
			}

			createTransition(loop.getExpression(), actualState, loop.getToState());

			Set<Transition> lastsEvenWithETransition = lastTransitionsBefore(loop, loop.getFromState());

			for (Transition lastEvenWithEpsilonTransition : lastsEvenWithETransition) {

				if (!lastEvenWithEpsilonTransition.expressionEqualsEpsilon()) {
					createTransition(lastEvenWithEpsilonTransition.getExpression(), lastEvenWithEpsilonTransition.getFromState(), actualState);
				} else {
					createTransitionBetweenLastBeforeEpsilonTransitionAndActualState(loop, actualState, lastEvenWithEpsilonTransition, toRemove);
				}

				toRemove.add(lastEvenWithEpsilonTransition);
			}

			toRemove.add(loop);

		}

	}

	private void createTransitionBetweenLastBeforeEpsilonTransitionAndActualState(Transition loopNodeTransition,
			Integer actualState, Transition lastEvenWithEpsilonTransition, LinkedList<Transition> toRemove) {
		
		List<Transition> trans = transitions.findTransitionsToState(lastEvenWithEpsilonTransition.getFromState());

		for (Transition t : trans) {
			createTransition(t.getExpression(), t.getFromState(), actualState);
		}

		if (trans.isEmpty()) {
			LinkedList<Transition> nextLoopNodeTransitions = transitions
					.findTransitionsFromState(loopNodeTransition.getToState());
			for (Transition nextLoopNodeTransition : nextLoopNodeTransitions) {
				createTransitionBetweenLastBeforeEpsilonTransitionAndActualState(nextLoopNodeTransition, actualState,
						lastEvenWithEpsilonTransition, toRemove);
			}
		}
	}

	private void fillNoloopAndLoopLists(int actualState, LinkedList<Transition> noLoopTransitions,
			LinkedList<Transition> loopTransitions, Set<Integer> eclosesOnTheWay) {

		for (Integer ecloseOnTheWay : eclosesOnTheWay) {
			LinkedList<Transition> ecloseTransitions = transitions.findTransitionsFromState(ecloseOnTheWay);

			for (Transition k : ecloseTransitions) {

				if (k.expressionContains(Constant.EPSILON)) {
					continue;
				}

				if (isCycle(k)) {
					loopTransitions.add(k);
				} else {
					noLoopTransitions.add(k);
				}

			}
		}
	}

}