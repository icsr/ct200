package br.ita.ct200.automatabuilder.exception;

public class RegexException extends Exception {

	public RegexException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 335538623276951931L;

}
