package br.ita.ct200.automatabuilder;

import java.util.List;
import java.util.Set;

import br.ita.ct200.automatabuilder.exception.RegexException;
import br.ita.ct200.automatabuilder.support.Constant;
import br.ita.ct200.automatabuilder.support.Tools;

public class NonDeterministicFiniteAutomatonWithEpsilonsTransitions extends Automaton {

	public NonDeterministicFiniteAutomatonWithEpsilonsTransitions(String regex) throws RegexException {
		this(regex, null);
	}

	public NonDeterministicFiniteAutomatonWithEpsilonsTransitions(String regex, Set<Character> alphabet)
			throws RegexException {
		super(regex, "AFN-&", alphabet);
	}

	@Override
	protected void build() {
		initialState = createState(false);
		finalState = createState(true);

		prepare(regex, initialState, finalState);

	}

	private void prepare(String regex, int fromState, int toState) {

		if (regex.length() < 1) {
			return;
		}

		if (regex.length() == 1) {
			createTransition(regex, fromState, toState);
			return;
		}

		if (hasNoUnionsOuterOfParenthesis(regex)) {
			perform(regex, fromState, toState);
			return;
		}

		List<String> splits = Tools.splitUnionsOutOfParenthesis(regex);
		for (String s : splits) {
			prepare(s, fromState, toState);
		}

	}

	private void perform(String regex, int fromState, int toState) {

		int last = fromState;

		int i = 0;
		while (i < regex.length() - 1) {
			char c = regex.charAt(i);

			int finalPosition = i + 1;

			if (c == Constant.OPEN_PARENTHESIS) {
				finalPosition = Tools.indexOfCloseParenthesis(regex, i) + 1;
			}

			String s = regex.substring(i, finalPosition);
			if (finalPosition < regex.length() && regex.charAt(finalPosition) == Constant.STAR) {
				finalPosition++;

				last = createEpsilonTransition(last);
				createLoopTransition(s, last);

			} else {
				int q = createState(false);
				createTransition(s, last, q);
				last = q;
			}

			i = finalPosition;

		}

		if (i == regex.length()) {
			createEpsilonTransition(last, toState);
		} else {
			createTransition("" + regex.charAt(regex.length() - 1), last, toState);
		}

	}

	private int createEpsilonTransition(int fromToState) {
		int q = createState(false);
		createTransition("" + Constant.EPSILON, fromToState, q);
		return q;
	}

	private void createEpsilonTransition(int fromToState, int toState) {
		createTransition("" + Constant.EPSILON, fromToState, toState);
	}

	protected void createLoopTransition(String expression, int last) {
		createTransition(Tools.removeOuterStar(expression), last, last);
	}

	@Override
	protected void createTransition(String expression, Integer fromState, Integer toState) {
		
		if(!states.contains(fromState) || !states.contains(toState)){
			return;
		}

		if (isParenthesisExpressionParenthesis(expression)) {
			prepare(Tools.removeOuterStarAndOuterParenthesis(expression), fromState, toState);
			return;
		}

		if (expression.length() == 0) {
			return;
		}

		transitions.add(expression, fromState, toState);

	}

}