package br.ita.ct200.automatabuilder.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Pattern;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import br.ita.ct200.automatabuilder.Automaton;
import br.ita.ct200.automatabuilder.NonDeterministicFiniteAutomatonWithEpsilonsTransitions;
import br.ita.ct200.automatabuilder.exception.RegexException;

public class Tools {

	private static List<Integer> findUnions(String regex) {

		Stack<Character> stack = new Stack<Character>();
		ArrayList<Integer> list = new ArrayList<Integer>();

		for (int i = 0; i < regex.length(); i++) {

			if (regex.charAt(i) == Constant.OPEN_PARENTHESIS) {
				stack.push(regex.charAt(i));
			}

			if (regex.charAt(i) == Constant.CLOSE_PARENTHESIS) {
				stack.pop();
			}

			if (regex.charAt(i) == Constant.UNION && stack.isEmpty()) {
				list.add(i);
			}

		}

		return list;

	}

	public static List<String> splitUnionsOutOfParenthesis(String regex) {

		List<Integer> list = findUnions(regex);
		List<String> splits = new ArrayList<String>();

		if (list.size() == 0) {
			splits.add(regex);
			return splits;
		}

		splits.add(regex.substring(0, list.get(0)));

		int i = 0;
		for (i = 0; i < list.size() - 1; i++) {
			splits.add(regex.substring(list.get(i) + 1, list.get(i + 1)));

		}

		splits.add(regex.substring(list.get(i) + 1, regex.length()));

		return splits;
	}

	public static List<Integer> findOcurrenciesOutOfParenthesis(String regex, char symbolToSearch) {

		List<Integer> list = new ArrayList<Integer>();

		int i = 0;
		while (true) {
			int unionPosition = regex.indexOf("" + Constant.UNION, i);
			if (unionPosition < 0) {
				break;
			}

			if (isParenthesisPaired(regex, 0, unionPosition)) {
				list.add(unionPosition);
			}

			i = unionPosition + 1;
		}

		return list;
	}

	public static String[] split(String expression) {

		String[] str = new String[expression.length()];
		for (int i = 0; i < expression.length(); i++) {
			str[i] = "" + expression.charAt(i);
		}

		return str;
	}

	public static void validate(String regex, Set<Character> alphabet, boolean includeSpecialSymbols)
			throws RegexException {

		if (regex == null) {
			throw new RegexException("Regex nula");
		}

		validatePattern(regex, alphabet, includeSpecialSymbols);

		validateParenthesisPairing(regex);

	}

	public static void validateParenthesisPairing(String regex) throws RegexException {
		if (!isParenthesisPaired(regex, 0, regex.length())) {
			throw new RegexException("Parênteses não emparelhados corretamente.");
		}
	}

	public static boolean isParenthesisPaired(String regex, int fromPosition, int toPosition) {

		Stack<Character> stack = new Stack<Character>();

		for (int i = fromPosition; i < toPosition; i++) {
			if (regex.charAt(i) == Constant.OPEN_PARENTHESIS) {
				stack.push(regex.charAt(i));
			}

			if (regex.charAt(i) == Constant.CLOSE_PARENTHESIS) {
				if (stack.isEmpty() || stack.pop() != Constant.OPEN_PARENTHESIS) {
					return false;
				}
			}
		}

		if (!stack.isEmpty()) {
			return false;
		}

		return true;

	}

	public static void validatePattern(String regex, Set<Character> alphabet, boolean includeSpecialSymbols)
			throws RegexException {
		StringBuilder str = new StringBuilder();
		str.append("[").append(Tools.enlist(alphabet));
		if (includeSpecialSymbols)
			str.append("\\*\\+\\(\\)");
		str.append("]*");

		if (!Pattern.matches(str.toString(), regex)) {
			throw new RegexException("Regex inválido");
		}
	}

	public static String enlist(Set<Character> alphabet) {

		return enlist(alphabet, null);
	}

	public static String enlist(Set<Character> alphabet, Character separator) {

		StringBuilder str = new StringBuilder();
		Iterator<Character> it = alphabet.iterator();
		while (it.hasNext()) {
			str.append(it.next());
			if (separator != null && it.hasNext())
				str.append(separator);
		}
		return str.toString();
	}

	public static double multiply(double... operands) {

		double ret = 1;

		for (double op : operands) {
			ret *= op;
		}

		return ret;
	}

	public static String removeOuterStarAndOuterParenthesis(String expression) {
		
		expression = removeOuterStar(expression);
		if(!expression.startsWith(""+Constant.OPEN_PARENTHESIS) || !expression.endsWith(""+Constant.CLOSE_PARENTHESIS)){
			return expression;
		}
		
		int indexOfOpenParenthesis = expression.indexOf(Constant.OPEN_PARENTHESIS );
		return expression.substring(indexOfOpenParenthesis + 1, Tools.indexOfCloseParenthesis(expression, indexOfOpenParenthesis));
		
		
	}

	public static String removeOuterStar(String expression) {
		if(!expression.endsWith(""+Constant.STAR)){
			return expression;
		}
		
		return expression.substring(0, expression.lastIndexOf(Constant.STAR));
	}

	
	public static String showInputMessage(String message) {
		return JOptionPane.showInputDialog(null, createLabel(message), Constant.APP_TITLE, JOptionPane.PLAIN_MESSAGE);
	}

	private static JLabel createLabel(String message) {
		
		JLabel label = new JLabel();
		label.setFont(Constant.DEFAULT_FONT);
		label.setHorizontalAlignment(JLabel.CENTER);
		
		StringBuilder str = new StringBuilder();
		str.append("<html><center>");
		str.append(message.replaceAll("\n", "<br/>"));
		str.append("</center></html>");
		label.setText(str.toString());
		
		return label;
	}

	public static void showMessage(String message) {
		JOptionPane.showMessageDialog(null, createLabel(message), Constant.APP_TITLE, JOptionPane.INFORMATION_MESSAGE);
	}

	//	public static LinkedList<Key<Integer, String>> removeAndGetTransitionsAgain(
	//			Map<Key<Integer, String>, List<Integer>> transitions, LinkedList<Key<Integer, String>> transitionsListToRemove,
	//			int fromState) {
	//
	//		for (Key<Integer, String> k : transitionsListToRemove) {
	//			transitions.remove(k);
	//		}
	//
	//		transitionsListToRemove.clear();
	//
	//		return Tools.findTransitions(transitions, fromState);
	//
	//	}

	public static List<Character> toList(String string) {
		List<Character> list = new ArrayList<Character>();

		for (char i : string.toCharArray()) {
			list.add(i);
		}
		return list;
	}

	public static Stack<Character> toStack(String string) {
		Stack<Character> stack = new Stack<Character>();

		for (char s : string.toCharArray()) {
			stack.push(s);
		}
		return stack;
	}

	public static List<Integer> toList(int... initialState) {
		List<Integer> list = new ArrayList<Integer>();

		for (Integer i : initialState) {
			list.add(i);
		}
		return list;
	}
	
	public static Set<Integer> toSet(Integer... initialState) {
		Set<Integer> set = new HashSet<Integer>();

		for (Integer i : initialState) {
			set.add(i);
		}
		return set;
	}

	public static List<Integer> clone(List<Integer> states) {

		List<Integer> clone = new LinkedList<Integer>();

		for (Integer s : states) {
			clone.add(new Integer(s));
		}

		return clone;
	}

	public static void normalizingAutomaton(Automaton auto){

		Automaton automata = auto;

		Transition startTransition = new Transition();
		startTransition.setFromState(Constant.INITIAL_STATE);		
		startTransition.setExpression("&");
		startTransition.setToState(0);

		automata.getTransitions().add(startTransition);
		
		Transition finalTransition;

		finalTransition = new Transition();
		finalTransition.setFromState(automata.getFinalState());
		finalTransition.setExpression("&");
		finalTransition.setToState(Constant.FINAL_STATE);		
		automata.getTransitions().add(finalTransition);

		int size = automata.getStates().size();


		for(int i = 0; i < size; i++){

			if(isKleeneStar(automata.getStates().get(i), automata)){				
				plusKleeneStar(automata.getStates().get(i), automata);
			} 
		}
		
		for(int i = 0; i < size; i++){

			if(isUnionNode(automata.getStates().get(i), automata)){
				unionValues(automata.getStates().get(i), automata);
			} 
		}

	}

	public static Automaton plusKleeneStar(Integer state,Automaton automata){

		String expressionResult = "";
		int size = automata.getTransitions().size();
		int currentToState = 0;
		int counter = 0;
		int counter2 = 0;

		for(int i = 0 ; i < size ;i++){

			if(automata.getTransitions().get(i).getFromState() == state){	

				currentToState = automata.getTransitions().get(i).getToState();

				for(int j = 0 ; j < size ;j++){

					if(automata.getTransitions().get(j).getFromState() == state && 
							automata.getTransitions().get(i).getToState() == currentToState	){

						counter2++;

						if(counter2 > 1)
							counter++;
					}
				}
			}

			if(counter > 0) {

				while(counter > 0){

					for(int index = 0 ; index < size ;index++){

						if(automata.getTransitions().get(index).getFromState() == state && 
								automata.getTransitions().get(index).getToState() == currentToState	){

							if(counter > 1){
								expressionResult += automata.getTransitions().get(index).getExpression() + "+";														
							} else
								expressionResult += automata.getTransitions().get(index).getExpression();		

							automata.getTransitions().remove(index);
							size = automata.getTransitions().size();
							break;
						}

					}

					counter--;
				}

				Transition t = new Transition();
				t.setExpression(expressionResult);
				t.setToState(currentToState);
				t.setFromState(state);
				automata.getTransitions().add(t);

			}

		}


		return automata;
	}

	public static Automaton getRegexFromAutomaton(Automaton auto) throws RegexException{

		Automaton automata = new NonDeterministicFiniteAutomatonWithEpsilonsTransitions(auto.getRegex());

		normalizingAutomaton(automata);

		int size = automata.getStates().size();
		
		for(int i = 0; i < size; i++){
			toKleeneStar(automata.getStates().get(i), automata);
		}

		for(int i = size-1; i >= 0; i--){
			
			if(isKleeneStar(automata.getStates().get(i), automata))
				toKleeneStar(automata.getStates().get(i), automata);
			
			processAutomaton(automata.getStates().get(i), automata);
			
		}

		automata.getStates().add(Constant.INITIAL_STATE);
		automata.getStates().add(Constant.FINAL_STATE);
		automata.getFinalStates().removeFirst();
		automata.getFinalStates().add(Constant.FINAL_STATE);

		size = automata.getStates().size();
	
		for(int i = 0; i < size; i++){
			unionValues(automata.getStates().get(i), automata);
		}	

		showTransitions(automata);
		
		return automata;
		
	}
	
	public static void showTransitions(Automaton automata){
		
		int size = automata.getTransitions().size();

		for(int i = 0; i < size; i++){
			System.out.println(automata.getTransitions().get(i).getFromState() + " "
					+ automata.getTransitions().get(i).getExpression() + " "
					+ automata.getTransitions().get(i).getToState());
		}

		System.out.println("\n");

		
	}

	public static Automaton processAutomaton(Integer state,Automaton automata){

		int size = automata.getTransitions().size();

		int counter = 0;

		List<Transition> fromStateList = new ArrayList<Transition>();
		List<Transition> kleeneStar = new ArrayList<Transition>();	
		List<Transition> toStateList = new ArrayList<Transition>();		

		/****** From State ******/

		for(int i = 0; i < size ; i++){

			if(automata.getTransitions().get(i).getToState() == state && 
					automata.getTransitions().get(i).getFromState() != state){

				Transition t = new Transition();
				t.setFromState(automata.getTransitions().get(i).getFromState());

				if(isKleeneStar(state, automata))
					t.setExpression(automata.getTransitions().get(i).getExpression()+getKleeneStarValue(state, automata));
				else
					t.setExpression(automata.getTransitions().get(i).getExpression());

				fromStateList.add(t);				
				counter++;
			}

		}

		while(counter > 0){
			for(int i = 0; i < size ; i++){
				if(automata.getTransitions().get(i).getToState() == state && 
						automata.getTransitions().get(i).getFromState() != state){
					automata.getTransitions().remove(i);
					break;
				}
			}
			counter--;
		}

		/****** Kleene Star ******/

		size = automata.getTransitions().size();

		for(int i = 0; i < size ; i++){

			if(automata.getTransitions().get(i).getToState() == state && 
					automata.getTransitions().get(i).getFromState() == state){

				Transition t = new Transition();
				t.setFromState(automata.getTransitions().get(i).getFromState());
				t.setExpression(automata.getTransitions().get(i).getExpression());
				kleeneStar.add(t);
				counter++;
			}
		}

		while(counter > 0){
			for(int i = 0; i < size ; i++){
				if(automata.getTransitions().get(i).getToState() == state && 
						automata.getTransitions().get(i).getFromState() == state){
					automata.getTransitions().remove(i);
					break;
				}
			}
			counter--;
		}

		/****** To State ******/

		size = automata.getTransitions().size();

		for(int i = 0; i < size ; i++){

			if(automata.getTransitions().get(i).getFromState() == state && 
					automata.getTransitions().get(i).getToState() != state){
				Transition t = new Transition();
				t.setToState(automata.getTransitions().get(i).getToState());
				t.setExpression(automata.getTransitions().get(i).getExpression());
				toStateList.add(t);
				counter++;
			}
		}

		while(counter > 0){
			for(int i = 0; i < size ; i++){
				if(automata.getTransitions().get(i).getFromState() == state && 
						automata.getTransitions().get(i).getToState() != state){
					automata.getTransitions().remove(i);
					break;
				}
			}
			counter--;
		}

		/****** Making Connections ******/

		if(toStateList.size() == fromStateList.size()){

			for(int i = 0; i < fromStateList.size() ; i++){
				for(int j = 0; j < toStateList.size() ; j++){
					Transition t = new Transition();
					t.setFromState(fromStateList.get(i).getFromState());

					if(toStateList.get(j).getExpression().contains("+") && !toStateList.get(j).getExpression().contains(")"))
						t.setExpression(fromStateList.get(i).getExpression()+"("+ toStateList.get(j).getExpression() + ")");
					else
						t.setExpression(fromStateList.get(i).getExpression()+toStateList.get(j).getExpression());
					
					t.setToState(toStateList.get(j).getToState());
					automata.getTransitions().add(t);

				}
			}

		} else if (toStateList.size() < fromStateList.size()){

			for(int i = 0; i < fromStateList.size() ; i++){
				Transition t = new Transition();
				t.setFromState(fromStateList.get(i).getFromState());
				
				if(toStateList.get(0).getExpression().contains("+") && !toStateList.get(0).getExpression().contains(")") )
					t.setExpression(fromStateList.get(i).getExpression()+ "("+toStateList.get(0).getExpression()+")");
				else
					t.setExpression(fromStateList.get(i).getExpression()+toStateList.get(0).getExpression());
				
				t.setToState(toStateList.get(0).getToState());
				automata.getTransitions().add(t);				
			}

		} else if (toStateList.size() > fromStateList.size()){

			if(fromStateList.size() > 0){

				for(int i = 0; i < toStateList.size() ; i++){
					Transition t = new Transition();
					t.setFromState(fromStateList.get(0).getFromState());
					
					if(toStateList.get(i).getExpression().contains("+") && !toStateList.get(i).getExpression().contains(")"))
						t.setExpression(fromStateList.get(0).getExpression()+"("+toStateList.get(i).getExpression()+")");
					else
						t.setExpression(fromStateList.get(0).getExpression()+toStateList.get(i).getExpression());
					
					t.setExpression(fromStateList.get(0).getExpression()+toStateList.get(i).getExpression());
					t.setToState(toStateList.get(i).getToState());
					automata.getTransitions().add(t);				
				}
			}

		}

		size = automata.getTransitions().size();		

		return automata;


	}


	public static Automaton unionValues(Integer state,Automaton automata){

		String expressionResult = "";
		int size = automata.getTransitions().size();
		int currentToState = 0;
		int counter = 0;

		for(int i = 0 ; i < size ;i++){

			if(automata.getTransitions().get(i).getFromState() == state){

				currentToState = automata.getTransitions().get(i).getToState();

				for(int j = 0 ; j < size ;j++){

					if(automata.getTransitions().get(j).getFromState() == state && 
							automata.getTransitions().get(j).getToState() == currentToState	){
							counter++;
					}
				}				
			}
			
			if(counter > 0) {

				while(counter > 0){

					for(int index = 0 ; index < size ;index++){

						if(automata.getTransitions().get(index).getFromState() == state && 
								automata.getTransitions().get(index).getToState() == currentToState	){

							if(counter > 1)
								expressionResult += automata.getTransitions().get(index).getExpression() + "+";
							else
								expressionResult += automata.getTransitions().get(index).getExpression();

							automata.getTransitions().remove(index);
							size = automata.getTransitions().size();
							break;
						}

					}

					counter--;
				}
				
				Transition t = new Transition();
				t.setExpression(expressionResult);
				t.setToState(currentToState);
				t.setFromState(state);
				automata.getTransitions().add(t);

			}

		}


		return automata;
	}

	public static Automaton concatenateValues(Integer state,Automaton automata){

		String expressionResult = "";
		int size = automata.getTransitions().size();		
		int counter = 0;

		Transition t = new Transition();

		for(int i = 0 ; i < size ;i++){

			if(automata.getTransitions().get(i).getToState() == state 
					&& state != automata.getTransitions().get(i).getFromState()){

				expressionResult += automata.getTransitions().get(i).getExpression();
				t.setFromState(automata.getTransitions().get(i).getFromState());
			}
		} 

		for(int i = 0 ; i < size ;i++){

			if(automata.getTransitions().get(i).getFromState() == state 
					&& state == automata.getTransitions().get(i).getToState()){

				expressionResult += automata.getTransitions().get(i).getExpression();
				counter++;

			}
		}

		for(int i = 0 ; i < size ;i++){

			if(automata.getTransitions().get(i).getFromState() == state 
					&& state != automata.getTransitions().get(i).getToState()){

				counter++;
				expressionResult += automata.getTransitions().get(i).getExpression();				
				t.setToState(automata.getTransitions().get(i).getToState());

			}
		}

		if(counter > 0){

			while(counter > 0){

				for(int index = 0 ; index < size ;index++){
					if(automata.getTransitions().get(index).getFromState() == state){
						automata.getTransitions().remove(index);
						size = automata.getTransitions().size();
						break;
					}
				}

				for(int index = 0 ; index < size ;index++){
					if(automata.getTransitions().get(index).getToState() == state){
						automata.getTransitions().remove(index);
						size = automata.getTransitions().size();
						break;
					}
				}

				counter--;
			}

			t.setExpression(expressionResult);				
			automata.getTransitions().add(t);

		}

		return automata;
	}


	public static boolean isUnionNode(Integer state,Automaton automata){

		boolean result = false;

		int size = automata.getTransitions().size();

		int currentToState = 0;

		int counter = 0;
		int counter2 = 0;

		for(int i = 0 ; i < size ;i++){

			if(automata.getTransitions().get(i).getFromState() == state && 
					!isKleeneStar(state, automata)){

				currentToState = automata.getTransitions().get(i).getToState();

				for(int j = 0 ; j < size ;j++){

					if(automata.getTransitions().get(j).getFromState() == state && 
							automata.getTransitions().get(j).getToState() == currentToState){

						if(!isKleeneStar(currentToState, automata)){
							counter2++;
							if(counter2 > 1)
								counter++;
						}
					}
				}
				
				counter2 = 0;
			}
		}

		if(counter > 0)
			result = true;

		return result;
	}

	public static boolean isKleeneStar(Integer state,Automaton automata){

		int size = automata.getTransitions().size();		

		boolean result = false;

		for(int i = 0 ; i < size ;i++){

			if(automata.getTransitions().get(i).getFromState() == state && 
					automata.getTransitions().get(i).getToState() == state){
				result = true;
				break;
			}
		}		

		return result; 
	}

	public static String getKleeneStarValue(Integer state,Automaton automata){

		int size = automata.getTransitions().size();		

		String result = "";

		for(int i = 0 ; i < size ;i++){

			if(automata.getTransitions().get(i).getFromState() == state && 
					automata.getTransitions().get(i).getToState() == state){
				result = automata.getTransitions().get(i).getExpression();				
				break;
			}
		}		

		return result; 
	}



	public static Automaton toKleeneStar(Integer state,Automaton automata){

		String expressionResult = "";
		int size = automata.getTransitions().size();		
		int counter = 0;

		Transition t = new Transition();

		for(int i = 0 ; i < size ;i++){

			if(automata.getTransitions().get(i).getFromState() == state && 
					automata.getTransitions().get(i).getToState() == state){

				t.setFromState(state);
				t.setToState(state);

				if(automata.getTransitions().get(i).getExpression().contains("*"))
					expressionResult += automata.getTransitions().get(i).getExpression();
				else if(automata.getTransitions().get(i).getExpression().contains("+"))
					expressionResult += "("+ automata.getTransitions().get(i).getExpression() + ")*";
				else
					expressionResult += automata.getTransitions().get(i).getExpression() + "*";
				counter++;
			}
		}

		if(counter > 0){	

			while(counter > 0){

				for(int index = 0 ; index < size ;index++){
					if(automata.getTransitions().get(index).getFromState() == state && 
							automata.getTransitions().get(index).getToState() == state	){

						automata.getTransitions().remove(index);
						size = automata.getTransitions().size();
						break;
					}
				}

				counter--;
			}

			t.setExpression(expressionResult);				
			automata.getTransitions().add(t);

		}

		return automata;
	}

	public static int indexOfCloseParenthesis(String regex, int indexOfOpenParenthesis) {
		
		if(regex.charAt(indexOfOpenParenthesis) != Constant.OPEN_PARENTHESIS)
			return -1;
		
		Stack<Character> stack = new Stack<Character>();
		
		int i = indexOfOpenParenthesis;
		stack.push(regex.charAt(i));
		while(++i < regex.length()){
			if(regex.charAt(i) == Constant.OPEN_PARENTHESIS){
				stack.push(regex.charAt(i));
			}
			
			if(regex.charAt(i) == Constant.CLOSE_PARENTHESIS){
				stack.pop();
				if(stack.isEmpty()){
					return i;
				}
			}
		}
		
		return -1;
	}

}
