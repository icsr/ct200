package br.ita.ct200.automatabuilder.exception;

public class AbortException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5492628587681350044L;

	public AbortException(String message) {
		super(message);
	}

}
