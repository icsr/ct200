package br.ita.ct200.automatabuilder.support;

import java.awt.Font;

public class Constant {
	
	public static final char UNION = '+';
	public static final char CLOSE_PARENTHESIS = ')';
	public static final char OPEN_PARENTHESIS = '(';
	public static final char STAR = '*';
	public static final char EPSILON = '&';
	public static final Font DEFAULT_FONT = new Font("Arial", Font.PLAIN, 16);
	public static final String APP_TITLE = "ITA / CT-200 - Prof. Forster - Automata Builder - Discentes: Israel C.S. Rocha e Filipe S. Queiroz";
	public static final String DEFAULT_REGEX = "(a(b+c))*"; //"aa(a+b)*c*b(aa)*"; "a*b+d*cc+(a+b)*c*b(aa)*+b*+c*";
	public static final String TMP_FOLDER = System.getProperty("java.io.tmpdir");
	public static final Integer INITIAL_STATE = -1;
	public static final Integer FINAL_STATE = -2;

}
