package br.ita.ct200.automatabuilder.support.view;

import java.io.File;

import br.ita.ct200.automatabuilder.Automaton;
import br.ita.ct200.automatabuilder.support.Transition;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.engine.GraphvizV8Engine;

public class Graph {

	private Automaton automata;

	public Graph(Automaton automata) {
		this.automata = automata;
	}

	public String toDotGraphvizFormat() {

		StringBuilder str = new StringBuilder("digraph {\n");
		str.append("\trankdir=LR;\n");

		str.append("\tnode[shape = \"circle\"];\n");

		for (Integer i : automata.getFinalStates()) {
			str.append(String.format("\t%d[shape = \"doublecircle\"];\n\n", i));
		}

		for (Transition k : automata.getTransitions()) {
				str.append(String.format("\t%d -> %d[label=\"%s\",weight=\"%s\"];\n", k.getFromState(), k.getToState(),
						k.getExpression(), k.getExpression()));

		}

		str.append("\n}");
		
		return str.toString();

	}

	public void renderToFile(File pngFile) {

		Graphviz.useEngine(new GraphvizV8Engine());
		Graphviz.initEngine();
		Graphviz.fromString(toDotGraphvizFormat()).renderToFile(pngFile);
		Graphviz.releaseEngine();
		
	}

}