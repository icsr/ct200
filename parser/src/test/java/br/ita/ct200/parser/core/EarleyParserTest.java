package br.ita.ct200.parser.core;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import br.ita.ct200.parser.aux.Outcome;
import br.ita.ct200.parser.exceptions.EarleyParserException;
import br.ita.ct200.parser.exceptions.GrammarException;

public class EarleyParserTest {

	Grammar grammarOne, grammarTwo;
	private Grammar grammarThree;
	private Grammar palindromeGramar;
	private Grammar grammarFour;
	private Grammar grammarZero;

	@Before
	public void setUp() throws Exception {
		
		grammarZero = new Grammar();
		grammarZero.addAlphabet("1", "2", "3", "4", "+", "*").addSymbols("S", "T", "M");
		grammarZero.addProduction("S", "S + M");
		grammarZero.addProduction("S", "M");
		grammarZero.addProduction("M", "M * T");
		grammarZero.addProduction("M", "T");
		grammarZero.addProduction("T", "1");
		grammarZero.addProduction("T", "2");
		grammarZero.addProduction("T", "3");
		grammarZero.addProduction("T", "4");
		
		grammarOne = new Grammar();
		grammarOne.addAlphabet("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "+", "*");
		grammarOne.addSymbols("S", "T", "M");
		grammarOne.addProduction("S", "S + M");
		grammarOne.addProduction("S", "M");
		grammarOne.addProduction("M", "M * T");
		grammarOne.addProduction("M", "T");
		grammarOne.addProduction("T", "1");
		grammarOne.addProduction("T", "2");
		grammarOne.addProduction("T", "3");
		grammarOne.addProduction("T", "4");
		grammarOne.addProduction("T", "5");
		grammarOne.addProduction("T", "6");
		grammarOne.addProduction("T", "7");
		grammarOne.addProduction("T", "8");
		grammarOne.addProduction("T", "9");
		grammarOne.addProduction("T", "0");
		grammarOne.addProduction("T", "TT");
		
		grammarTwo = new Grammar(grammarOne);
		grammarTwo.addAlphabet("(", ")");
		grammarTwo.addProduction("M", "(M+M)").addProduction("M", "M * M");
		
		grammarThree = new Grammar(grammarTwo).addProduction("M", "M + M");
		
		grammarFour = new Grammar().addAlphabet("a", "b").addSymbols("S").addProduction("S", "abS").addProduction("S", "");
		
		palindromeGramar = new Grammar().addAlphabet("a", "b").addSymbols("S");
		palindromeGramar.addProduction("S", "a").addProduction("S", "b").addProduction("S", Grammar.EPSILON);
		palindromeGramar.addProduction("S", "aSa").addProduction("S", "bSb");


			
	}

	@Test
	public void testGoodInputs() throws EarleyParserException {
		performTest(grammarOne, "2 + 3 * 4", true);
		performTest(grammarOne, "22 + 3 * 4", true);
		performTest(grammarOne, "22099 + 345 * 499", true);
		
		performTest(grammarTwo, "(2 + 3) * 4", true);
		performTest(grammarTwo, "(200 + 33) * (4 + 55)", true);
		
		performTest(grammarThree, "(200 + 33 + 4) * (4 + 55)", true);
		performTest(grammarThree, "(200 + 33 + 4) * (4 + 55 + 2 * 10)", true);
		
		performTest(palindromeGramar, "", true);
		performTest(palindromeGramar, "aba", true);
		performTest(palindromeGramar, "aabaa", true);
		
		performTest(grammarFour, "", true);		
		performTest(grammarFour, "ab", true);
		performTest(grammarFour, "abab", true);
		performTest(grammarFour, "ababab", true);

		performTest(palindromeGramar, "aaaabbbaaaa", true);
		performTest(palindromeGramar, "aaaabbaabbaaaa", true);
				
	}
	
	@Test
	public void testGrammarZero() throws EarleyParserException, GrammarException {
		Outcome o = performTest(grammarZero, "2 + 3 * 4", true);
		System.out.println(o.toString(true));
	}
	
	
	@Test
	public void testBadInputs() throws EarleyParserException {
		performTest(grammarOne, "2 ++ 3 * 4", false);
		performTest(grammarOne, "(2 + 3) * 4", false);
		performTest(grammarOne, "2aa + 3 * 4", false);
		performTest(grammarOne, "2 + 3 ** 4", false);
		
		performTest(grammarTwo, "(2 + 3 * 4", false);
		performTest(grammarTwo, "(2 + 3)) * 4", false);
		performTest(grammarTwo, "(200 + 33 + 4) * (4 + 55)", false);
		
		performTest(grammarFour, "a", false);		
		performTest(grammarFour, "b", false);
		performTest(grammarFour, "ba", false);
		performTest(grammarFour, "aba", false);
		performTest(grammarFour, "abb", false);
		
		performTest(palindromeGramar, "aaabaa", false);
		performTest(palindromeGramar, "aaaabbabbbaaaa", false);
	}

	private Outcome performTest(Grammar grammar, String input, boolean acceptableInput) throws EarleyParserException{
		Outcome o = new EarleyParser(grammar).perform(input);
		if(acceptableInput){
			assertTrue(o.isInputAcceptedByGrammar());
		}else{
			assertFalse(o.isInputAcceptedByGrammar());
		}
		System.out.println(o);
		
		return o;
	}
}
