package br.ita.ct200.parser.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import br.ita.ct200.parser.exceptions.GrammarException;

public class GrammarTest {

	Grammar mainGrammar;
	private Grammar equivalentGrammar;

	@Before
	public void setUp() throws Exception {
		this.mainGrammar = new Grammar();

		mainGrammar.addAlphabet("1", "2", "3", "4", "+", "*");
		mainGrammar.addSymbols("S", "T", "M");
		mainGrammar.addProduction("S", "S + M");
		mainGrammar.addProduction("S", "M");
		mainGrammar.addProduction("M", "M * T");
		mainGrammar.addProduction("M", "T");
		mainGrammar.addProduction("T", "1");
		mainGrammar.addProduction("T", "2");
		mainGrammar.addProduction("T", "3");
		mainGrammar.addProduction("T", "4");

		
		this.equivalentGrammar = new Grammar();

		equivalentGrammar.addAlphabet("1", "2", "3", "4", "+", "*");
		equivalentGrammar.addSymbols("S", "T", "M");
		equivalentGrammar.addProduction("S", "M | S + M");
		equivalentGrammar.addProduction("M", "M * T | T");
		equivalentGrammar.addProduction("T", "1 | 2 | 3 | 4");

	}

	@Test
	public void testAddIncorrectAndCorrectProductions() {
		testAddIncorrectAndCorrectProductions(mainGrammar);
		testAddIncorrectAndCorrectProductions(equivalentGrammar);
	}

	private void testAddIncorrectAndCorrectProductions(Grammar grammar) {
		try {
			grammar.addProduction("M", "M * X");
			fail("Deveria ter lançado exceção, X não pertence à gramática");
		} catch (GrammarException e) {
			
		}
		
		try {
			grammar.addProduction("M", "M * T");
		} catch (GrammarException e) {
			fail("Não deveria ter lançado exceção.");			
		}
	}

	@Test
	public void testAddSameProduction() throws GrammarException {
		testAddSameProduction(mainGrammar);
		testAddSameProduction(equivalentGrammar);
	}

	private void testAddSameProduction(Grammar grammar) throws GrammarException {
		int previousSize = grammar.productions.size();
		Production sameProduction = grammar.productions.iterator().next();
		grammar.addProduction(sameProduction.leftSentence, sameProduction.rightSentence);
		assertEquals(previousSize, grammar.productions.size());
	}

}
