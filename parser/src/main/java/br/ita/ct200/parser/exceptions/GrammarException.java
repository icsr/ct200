package br.ita.ct200.parser.exceptions;

public class GrammarException extends Exception {

	public GrammarException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2209829701148330164L;

}
