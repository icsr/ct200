package br.ita.ct200.parser.core;

import java.util.LinkedList;
import java.util.List;

import br.ita.ct200.parser.aux.Logger;
import br.ita.ct200.parser.aux.Outcome;
import br.ita.ct200.parser.exceptions.EarleyParserException;

public class EarleyParser {

	private Grammar grammar;
	private LinkedList<Chart> charts = new LinkedList<Chart>();

	public EarleyParser(Grammar grammar) throws EarleyParserException {
		this.grammar = grammar;
	}

	public Outcome perform(String input) {

		input = input.replaceAll(Grammar.SPACE, "");
		charts.clear();

		addToChart(0, Grammar.RESERVED_SYMBOL, grammar.startSymbol, 0, 0, State.Action.STARTING, 0);

		for (int inputIndex = 0; inputIndex <= input.length() && inputIndex < charts.size(); inputIndex++) {

			Logger.append(">> Inspecionando Chart ", "" + inputIndex);

			Chart actualChart = charts.get(inputIndex);

			for (int stateIndexOnActualChart = 0; stateIndexOnActualChart < actualChart.getStates()
					.size(); stateIndexOnActualChart++) {
				State stateOnChart = actualChart.getState(stateIndexOnActualChart);

				if (stateOnChart.isCompleted()) {
					complete(stateOnChart, input, inputIndex, stateIndexOnActualChart);
					continue;
				}

				if (isNonTerminal(stateOnChart)) {
					predict(stateOnChart, input, inputIndex, stateIndexOnActualChart);
				} else {
					scan(stateOnChart, input, inputIndex, stateIndexOnActualChart);
				}

			}
		}

		return new Outcome(grammar, input, charts);

	}

	private void predict(State state, String input, int inputIndex, int stateIndexOnActualChart) {

		Logger.append("Predicting", state, inputIndex);

		for (Production p : getProductionsByLeftSentence(state)) {
			addToChart(inputIndex, p.leftSentence, p.rightSentence, 0, inputIndex, State.Action.PREDICTION, stateIndexOnActualChart);
		}

	}

	private void scan(State state, String input, int inputIndex, int stateIndexOnActualChart) {

		Logger.append("Scanning", state, inputIndex);

		if (!isCurrentInputCharEqualsStateCurrentRightSentenceChar(state, input, inputIndex)) {
			return;
		}

		addToChart(inputIndex + 1, state.getLeftSentence(), state.getRightSentence(),
				state.getRightSentenceCurrentPosition() + 1, state.getParentChartIndex(), State.Action.SCANNING, stateIndexOnActualChart);

	}

	private void complete(State state, String input, int inputIndex, int stateIndexOnActualChart) {

		Logger.append("Completing", state, inputIndex);

		for (State s : getChartCreatorOf(state).getStates(true)) {
			if (s.matchRightSentenceCharAtCurrentPosition(state.getLeftSentence())) {
				addToChart(inputIndex, s.getLeftSentence(), s.getRightSentence(),
						s.getRightSentenceCurrentPosition() + 1, s.getParentChartIndex(), State.Action.COMPLETION, stateIndexOnActualChart);
			}
		}

	}

	private void addToChart(int destinationChartIndex, String leftSentence, String rightSentence,
			int rightSentenceCurrentPosition, int originInputIndexPosition, State.Action action, int creatorIndex) {

		State newState = new State(new Production(leftSentence, rightSentence), rightSentenceCurrentPosition,
				originInputIndexPosition, action, creatorIndex);

		boolean added = true;

		if (!charts.isEmpty() && destinationChartIndex < charts.size() && charts.get(destinationChartIndex) != null) {
			added = charts.get(destinationChartIndex).addState(newState);
		} else {
			charts.add(destinationChartIndex, new Chart(newState));
		}

		if (added) {
			Logger.append("Adding", newState, destinationChartIndex);
		}

	}

	private Chart getChartCreatorOf(State state) {
		return charts.get(state.getParentChartIndex());
	}

	private boolean isCurrentInputCharEqualsStateCurrentRightSentenceChar(State state, String input, int inputIndex) {

		if (inputIndex >= input.length()) {
			return false;
		}

		return state.getRightSentenceCharAtCurrentPosition().equals("" + input.charAt(inputIndex));

	}

	private boolean isNonTerminal(State state) {
		return grammar.symbols.contains(state.getRightSentenceCharAtCurrentPosition());
	}

	private List<Production> getProductionsByLeftSentence(State state) {

		List<Production> list = new LinkedList<Production>();

		for (Production p : grammar.productions) {
			if (p.leftSentence.equals(state.getRightSentenceCharAtCurrentPosition())) {
				list.add(p);
			}
		}

		return list;
	}

}
