package br.ita.ct200.parser.core;

import java.io.Serializable;

import br.ita.ct200.parser.exceptions.GrammarException;

public class Grammar implements Serializable {

	private static final long serialVersionUID = 5305613162382481345L;

	public static final String OR_REGEX = "\\|";
	public static final String SPACE = "\\ ";
	public static final String RESERVED_SYMBOL = "@";
	
	private static final String[] RESERVED_SYMBOLS = {OR_REGEX, SPACE, RESERVED_SYMBOL};

	public static final String EPSILON = "";

	OrderedSet<String> symbols, alphabet;
	OrderedSet<Production> productions;
	String startSymbol;

	public Grammar() {
		this.alphabet = new OrderedSet<String>();
		this.symbols = new OrderedSet<String>();
		this.productions = new OrderedSet<Production>();
	}

	@SuppressWarnings("unchecked")
	public Grammar(Grammar grammar) {
		this.alphabet = (OrderedSet<String>) grammar.alphabet.clone();
		this.symbols = (OrderedSet<String>) grammar.symbols.clone();
		this.productions = (OrderedSet<Production>) grammar.productions.clone();
		this.startSymbol = grammar.startSymbol;
	}

	public Grammar addSymbols(String... symbols) {

		if (startSymbol == null) {
			startSymbol = symbols[0];
		}

		for (String s : symbols) {
			try {
				checkSymbol(s);
				this.symbols.add(s);
			} catch (GrammarException e) {
				e.printStackTrace();
			}
		}
		
		return this;
	}


	public Grammar addAlphabet(String... alphabet) {
		for (String a : alphabet) {
			try {
				checkSymbol(a);
				this.alphabet.add(a);
			} catch (GrammarException e) {
				e.printStackTrace();
			}
		}
		
		return this;
	}
	
	private void checkSymbol(String symbol) throws GrammarException {	
		for(String rs : RESERVED_SYMBOLS){
			if(symbol.equals(rs)){
				throw new GrammarException(String.format("Símbolo %s não é admitido como símbolo.", symbol));
			}
		}
	}

	public Grammar addProduction(String leftSentence, String rightSentence) throws GrammarException {
		for (String rs : rightSentence.split(OR_REGEX)) {
			productions.add(new Production(leftSentence, rs).check(this));
		}
		
		return this;

	}

	public void setStartSymbol(String symbol) {

		if (!symbols.contains(symbol)) {
			symbols.add(symbol);
		}

		this.startSymbol = symbol;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		for(Production p : productions){
			str.append(p).append("\n");
		}
		
		str.append("Start: ").append(startSymbol);

		return str.toString();
	}

}
