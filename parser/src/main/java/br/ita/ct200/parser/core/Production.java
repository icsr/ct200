package br.ita.ct200.parser.core;

import java.io.Serializable;

import br.ita.ct200.parser.exceptions.GrammarException;

public class Production implements Serializable {

	private static final long serialVersionUID = 7570942504337573480L;

	public static final String ARROW = " -> ";

	private static final String EPSILON = "\u03b5";

	String leftSentence;
	String rightSentence;

	public Production(String leftSentence, String rightSentence) {
		this.leftSentence = leftSentence.replaceAll(" ", "");
		this.rightSentence = rightSentence.replaceAll(" ", "");
		;
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Production) && (((Production) obj).leftSentence.equals(this.leftSentence)
				&& ((Production) obj).rightSentence.equals(this.rightSentence));
	}

	@Override
	public int hashCode() {
		return (int) (this.leftSentence.hashCode() + this.rightSentence.hashCode());
	}

	public Production check(Grammar grammar) throws GrammarException {
		check(grammar, leftSentence);
		check(grammar, rightSentence);
		return this;
	}

	public void check(Grammar grammar, String sentence) throws GrammarException {
		for (int i = 0; i < sentence.length(); i++) {
			if (!grammar.symbols.contains("" + sentence.charAt(i))
					&& !grammar.alphabet.contains("" + sentence.charAt(i))) {
				throw new GrammarException(String.format("Símbolo %s não partence â gramática.", sentence.charAt(i)));
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(leftSentence).append(ARROW).append(rightSentence.length() == 0 ? EPSILON : rightSentence);

		return str.toString();
	}

}
