package br.ita.ct200.parser.exceptions;

public class EarleyParserException extends Exception {

	private static final long serialVersionUID = -4806637885076825151L;

	public EarleyParserException(String format) {
		super(format);
	}

}
