package br.ita.ct200.parser.aux;

import br.ita.ct200.parser.core.State;

public class Logger {
	
	private static StringBuilder logger = new StringBuilder();
	
	public static void reset(){
		logger.setLength(0);
	}
	
	public static void append(String log){
		logger.append(log);
	}
	
	public static void append(String action, State state, int chartIndex){
		append(String.format("%s %s - Chart %d", action, state, chartIndex));
		append("\n");
	}
	
	public static void append(String... logs){
		for(String s : logs){
			append(s);
		}
		
		append("\n");
	}
	
	
	public static String getLog(){
		return logger.toString();
	}
	
	public static String extractLog(boolean reset){
		String str = getLog();
		if(reset){
			reset();
		}
		
		return str;
	}

}
