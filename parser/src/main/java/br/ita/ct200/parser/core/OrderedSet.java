package br.ita.ct200.parser.core;

import java.util.LinkedList;
import java.util.Set;

public class OrderedSet<T> extends LinkedList<T> implements Set<T> {

	private static final long serialVersionUID = -94600657405907317L;
	
	@Override
	public boolean add(T e) {
		if(this.contains(e)){
			return false;
		}
		
		return super.add(e);
	}
	
	

}
