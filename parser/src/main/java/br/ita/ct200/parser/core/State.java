package br.ita.ct200.parser.core;

public class State {

	public static final String DOT = ".";
	private Production production;
	private Integer rightSentenceCurrentPosition, originInputIndexPosition;

	public enum Action {
		PREDICTION("PREDICTION"), COMPLETION("COMPLETION"), SCANNING("SCANNING  "), STARTING("STARTING  ");
		private String desc;

		Action(String desc){
			this.desc = desc;
		}
		
		@Override
		public String toString() {
			return desc;
		}
	};

	private Action createdByAction;
	private int creatorIndex;

	public State(Production production, int rightSentenceCurrentPosition, int originInputIndexPosition,
			Action createdAction, int creatorIndex) {
		this.production = production;
		this.rightSentenceCurrentPosition = rightSentenceCurrentPosition;
		this.originInputIndexPosition = originInputIndexPosition;
		this.createdByAction = createdAction;
		this.creatorIndex = creatorIndex;
	}

	public boolean isCompleted() {
		return rightSentenceCurrentPosition >= production.rightSentence.length();
	}

	@Override
	public String toString() {

		StringBuilder str = new StringBuilder();

		str.append(String.format("%-20s", this.production.toString()));
		str.insert(
				this.rightSentenceCurrentPosition + this.production.leftSentence.length() + Production.ARROW.length(),
				DOT);

		str.append(" (Origin Pos: ").append(this.originInputIndexPosition).append(" - ").append(createdByAction).append(" at ").append(creatorIndex).append(")");

		return str.toString();
	}

	public String getRightSentenceCharAtCurrentPosition() {
		return "" + this.production.rightSentence.charAt(rightSentenceCurrentPosition);
	}

	public boolean matchRightSentenceCharAtCurrentPosition(String symbol) {

		if (rightSentenceCurrentPosition >= getRightSentence().length()) {
			return false;
		}

		return getRightSentenceCharAtCurrentPosition().equals(symbol);
	}

	@Override
	public boolean equals(Object obj) {

		return obj instanceof State && ((State) obj).rightSentenceCurrentPosition.equals(rightSentenceCurrentPosition)
				&& ((State) obj).originInputIndexPosition.equals(originInputIndexPosition)
				&& ((State) obj).production.equals(production);

	}

	@Override
	public int hashCode() {
		return (int) (production.hashCode() + originInputIndexPosition + rightSentenceCurrentPosition);
	}

	public Integer getRightSentenceCurrentPosition() {
		return rightSentenceCurrentPosition;
	}

	public Integer getParentChartIndex() {
		return originInputIndexPosition;
	}

	public String getLeftSentence() {
		return this.production.leftSentence;
	}

	public String getRightSentence() {
		return this.production.rightSentence;
	}

	public Action getCreatedByAction() {
		return createdByAction;
	}

	public int getCreatorIndex() {
		return creatorIndex;
	}

}
