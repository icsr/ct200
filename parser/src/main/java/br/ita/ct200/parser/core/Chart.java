package br.ita.ct200.parser.core;

import java.util.Collections;

public class Chart {

	private OrderedSet<State> states;

	public Chart(OrderedSet<State> states) {
		this.states = states;
	}

	public Chart() {
		this.states = new OrderedSet<State>();
	}

	public Chart(State state) {
		this();
		addState(state);
	}

	public boolean addState(State state) {
		return this.states.add(state);
	}

	@Override
	public boolean equals(Object obj) {

		return obj instanceof Chart && ((Chart) obj).states.equals(states);

	}

	@Override
	public int hashCode() {
		return (int) states.hashCode();
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		int counter = 0;
		for (State s : states) {
			str.append("\nIndex ").append(counter++).append(": ").append(s);
		}

		return str.toString();
	}

	public State getState(int index) {
		return states.get(index);
	}

	public OrderedSet<State> getStates() {
		return states;
	}

	public OrderedSet<State> getStates(boolean reversed) {

		if (!reversed) {
			return getStates();
		}

		@SuppressWarnings("unchecked")
		OrderedSet<State> s = (OrderedSet<State>) states.clone();
		Collections.reverse(s);
		return s;
	}

	public boolean isEmpty() {
		return states.isEmpty();
	}

	public State getLastState() {
		return this.states.getLast();
	}

	public boolean contains(State state) {
		return this.states.contains(state);
	}

	public Boolean isCompletedParse() {

		for (State s : getStates(true)) {
			if (s.getLeftSentence().equals(Grammar.RESERVED_SYMBOL) && s.getParentChartIndex().equals(0)) {
				return true;
			}
		}

		return false;
	}

}
