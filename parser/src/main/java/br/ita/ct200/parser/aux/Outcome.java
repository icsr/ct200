package br.ita.ct200.parser.aux;

import java.util.LinkedList;

import br.ita.ct200.parser.core.Chart;
import br.ita.ct200.parser.core.Grammar;

public class Outcome {

	Grammar grammar;
	LinkedList<Chart> charts;
	Boolean inputAcceptedByGrammar = null;
	String input;
	String log;

	public Outcome(Grammar grammar, String input, LinkedList<Chart> charts) {
		this.grammar = grammar;
		this.charts = charts;
		this.input = input;
		this.log = Logger.extractLog(true);

		checkAcceptance();
	}

	private void checkAcceptance() {
		inputAcceptedByGrammar = charts.getLast().isCompletedParse() && charts.size() == input.length() + 1;
	}

	public Grammar getGrammar() {
		return grammar;
	}

	public LinkedList<Chart> getCharts() {
		return charts;
	}

	public Boolean isInputAcceptedByGrammar() {
		return inputAcceptedByGrammar;
	}

	public String getInput() {
		return input;
	}

	public String toString(boolean appendCharts) {
		StringBuilder str = new StringBuilder();
		str.append("\n>> Outcome from Grammar:\n").append(this.grammar);
				
		str.append("\n>> Input ").append(input).append(" is").append(isInputAcceptedByGrammar() ? " " : " NOT ")
		.append("accepted by this Grammar.");

		if (appendCharts) {
			str.append("\n>> Produced Charts:");
			int counter = 0;
			for (Chart c : charts) {
				str.append(String.format("\n\nChart %d:", counter++));
				str.append(c.toString());
			}
		}


		return str.toString();
	}
	
	@Override
	public String toString() {
		return toString(false);
	}

}
